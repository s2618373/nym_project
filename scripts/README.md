# README.md for Nym Mixnet Project

## Project Overview
This project focuses on the Nym Mixnet, aiming to analyze and enhance the privacy of digital communications. The source code is tailored for Ubuntu 22.04.3-LTS, ensuring compatibility and smooth execution. You can access the entire project repository at [Nym Project GitLab](https://git.ecdf.ed.ac.uk/s2618373/nym_project). Alongside the source code, this repository includes a suite of scripts pivotal for testing and analyzing the Nym network.

## Scripts Description
The heart of the project lies in its scripts, each serving a unique role in managing network components, simulating traffic, and recording network activities. Below is a detailed overview of these scripts:

### /scripts/localnet_start_mod2.sh
- **Purpose**: Launches the Nym local network.
- **Usage**: Run `./localnet_start_mod2.sh` to initiate the network. To halt the operation, use Ctrl+C and execute `tmux kill-session -t localnet` to end the tmux session.

### /scripts/build_topology.py
- **Purpose**: Automatically generates the network topology, detailing mixnodes and gateways.
- **Note**: This script is automatically invoked by `localnet_start_mod2.sh`.

### /scripts/manage_interfaces.py
- **Purpose**: Offers the ability to selectively deactivate and reactivate network interfaces, testing the Nym network's robustness against changes.

### /scripts/test_connectivity.py
- **Purpose**: Simulates network traffic and analyzes it through a SOCKS5 proxy, assisting in understanding data flow within the Mixnet.

### /scripts/record_traffic.py
- **Purpose**: Captures real-time network traffic from designated interfaces, ideal for monitoring mixnodes activities.
- **Output**: Saves the traffic logs for in-depth analysis.

## Installation Requirements
Before running the scripts associated with the Nym network project, ensure the following dependencies are installed on your system:

- Python 3.8 or higher.
- A Python virtual environment is highly recommended for managing dependencies.
- Tcpdump for network traffic capture (use `sudo apt-get install tcpdump` on Debian/Ubuntu).
- Network Manager's command-line interface Nmcli, typically pre-installed on many Linux distributions.
- Curl for making web requests (use `sudo apt-get install curl`).
- jq for JSON processing (use `sudo apt-get install jq`).
- Git for source code version control (use `sudo apt-get install git`).

## Running the Scripts
The scripts `manage_interfaces.py`, `test_connectivity.py`, and `record_traffic.py` require elevated permissions. Execute them with the following command:
```bash
sudo python3 [script_name]
