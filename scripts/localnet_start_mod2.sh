#!/usr/bin/env bash

set -o errexit

# can't just use `mktemp` since syntax differs between linux and macos (thx apple)
suffix=$(openssl rand -base64 10 | tr -dc 'a-zA-Z0-9')
localnetdir="$HOME/.nym/localnets/localnet.$suffix"
mkdir -p "$localnetdir"

# set the log_file route
log_file="$localnetdir/nym_traffic_log.txt"

# Set RUST_LOG_STYLE to always for color output
export RUST_LOG_STYLE=always

echo "Using $localnetdir for the localnet"

#!/usr/bin/env bash

set -o errexit

# List available IP addresses
ip_addresses=($(hostname -I))
echo "List of available IP addresses:"
for ((i=0; i<${#ip_addresses[@]}; i++)); do
  echo "$i: ${ip_addresses[i]}"
done

# Ask the user for input
read -p "Do you want to use the listed local IP addresses for the mixnodes? (y/n): " choice

# Check the user's choice
if [ "$choice" == "y" ]; then
  echo "Proceeding with the listed IP addresses for the mixnodes. The gateway will have the localhost"
elif [ "$choice" == "n" ]; then
  echo "Using localhost IP (127.0.0.1) for the mixnodes and gateway..."
  for ((i=0; i<=3; i++)); do
    ip_addresses[$i]="127.0.0.1"
  done
else
  echo "Invalid choice. Please run the script again."
  exit 1
fi

echo "initialising mixnode1..."
RUST_LOG=debug cargo run --release --bin nym-mixnode -- init --id "mix1-$suffix" --host ${ip_addresses[0]} --mix-port 10001 --verloc-port 20001 --http-api-port 30001 --output=json >> "$localnetdir/mix1.json"

echo "initialising mixnode2..."
RUST_LOG=debug cargo run --release --bin nym-mixnode -- init --id "mix2-$suffix" --host ${ip_addresses[1]} --mix-port 10002 --verloc-port 20002 --http-api-port 30002 --output=json >> "$localnetdir/mix2.json"

echo "initialising mixnode3..."
RUST_LOG=debug cargo run --release --bin nym-mixnode -- init --id "mix3-$suffix" --host ${ip_addresses[2]} --mix-port 10003 --verloc-port 20003 --http-api-port 30003 --output=json >> "$localnetdir/mix3.json"

echo "initialising gateway..."
RUST_LOG=debug cargo run --release --bin nym-gateway -- init --id "gateway-$suffix" --host 127.0.0.1 --mix-port 10004 --clients-port 9000 --output=json >> "$localnetdir/gateway.json"
# RUST_LOG=debug cargo run --release --bin nym-gateway -- init --id "gateway-$suffix" --host ${ip_addresses[3]} --mix-port 10004 --clients-port 9000 --output=json >> "$localnetdir/gateway.json"

# build the topology
echo "combining json files..."
python3 build_topology.py "$localnetdir"
networkfile=$localnetdir/network.json
echo "the full network file is located at $networkfile"

# start up the mixnet
# control the ctrl+c command to exit the program, the script will trap the interrupt signal
trap 'tmux kill-session -t localnet' INT 
echo "starting the mixnet..."
tmux start-server

# modified RUST_LOG in order to see DEBUG logging and export to file
tmux new-session -d -s localnet -n Mixnet -d "script -q -c 'RUST_LOG=debug cargo run --release --bin nym-mixnode -- run --id mix1-$suffix' $log_file"
tmux split-window -t localnet:0 "script -q -c 'RUST_LOG=debug cargo run --release --bin nym-mixnode -- run --id mix2-$suffix' $log_file"
tmux split-window -t localnet:0 "script -q -c 'RUST_LOG=debug cargo run --release --bin nym-mixnode -- run --id mix3-$suffix' $log_file"
tmux split-window -t localnet:0 "script -q -c 'RUST_LOG=debug cargo run --release --bin nym-gateway -- run --id gateway-$suffix' $log_file"

# tmux split-window -t localnet:0 "/usr/bin/env sh -c \"RUST_LOG=debug cargo run --release --bin nym-gateway -- run --id gateway-$suffix\""

echo "waiting for nym-gateway to launch on port 9000..."
while ! nc -z localhost 9000; do
  sleep 2
done
echo "nym-gateway launched"

# initialise the clients
# network requester is responsible for handling or managing network requests within the created network environment. 
echo "initialising network requester..."
RUST_LOG=debug cargo run --release --bin nym-network-requester -- init --id "network-requester-$suffix" --open-proxy=true --custom-mixnet "$networkfile" --output=json >> "$localnetdir/network_requester.json"
address=$(jq -r .client_address "$localnetdir/network_requester.json")

echo "initialising socks5 client..."
RUST_LOG=debug cargo run --release --bin nym-socks5-client -- init --id "socks5-client-$suffix" --provider "$address" --custom-mixnet "$networkfile" --no-cover

# startup the clients
tmux new-window -t 1 -n 'Clients' -d "/usr/bin/env sh -c \" RUST_LOG=debug cargo run --release --bin nym-network-requester -- run --id network-requester-$suffix --custom-mixnet $networkfile \"; /usr/bin/env sh -i"
tmux split-window -t localnet:1 "/usr/bin/env sh -c \" RUST_LOG=debug cargo run --release --bin nym-socks5-client -- run --id socks5-client-$suffix --custom-mixnet $networkfile \"; /usr/bin/env sh -i"
tmux split-window -t localnet:1

# Define function to capture traffic with tcpdump
# capture_traffic() {
#    echo "Capturing traffic on loopback interface. Use Wireshark to analyze the capture file."
#    tcpdump -i lo -w "$localnetdir/traffic_capture.pcap" &
# }

# Capture traffic if needed
# read -p "Do you want to capture traffic for Wireshark analysis? (y/n): " capture_choice
# if [[ "$capture_choice" == "y" ]]; then
#     capture_traffic
# fi


# prepare the command to test the socks5
# tmux send-keys -t localnet:1 "time curl -x socks5h://127.0.0.1:1080 https://test-download-files-nym.s3.amazonaws.com/download-files/1MB.zip --output /dev/null 2>&1"

tmux select-layout -t localnet:0 tiled
tmux select-layout -t localnet:1 tiled

tmux attach -t localnet
