import subprocess
import re

def get_ip_address(interface_name):
    # Run the command to get the IP address of the interface
    result = subprocess.run(['ip', 'addr', 'show', interface_name], stdout=subprocess.PIPE)
    # Decode the result to text
    output = result.stdout.decode('utf-8')

    # Use a regular expression to find the IP address in the output
    ip_match = re.search(r'inet (\d+\.\d+\.\d+\.\d+)', output)
    if ip_match:
        return ip_match.group(1)
    return 'N/A'

def get_interfaces_status():
    # Run the command to get the network interfaces and their statuses
    result = subprocess.run(['nmcli', 'device', 'status'], stdout=subprocess.PIPE)
    # Decode the result to text
    output = result.stdout.decode('utf-8')
    
    # Split the output into lines and skip the header line
    lines = output.split('\n')[1:]
    
    # Parse the lines
    interfaces = []
    for line in lines:
        if line.strip():
            parts = line.split()
            device = parts[0]
            state = parts[2]
            connection = parts[3] if len(parts) >= 4 else 'N/A'
            ip_address = get_ip_address(device) if state == "connected" else 'N/A'
            interfaces.append((device, state, connection, ip_address))
    return interfaces

def change_interface_status(interface_name, action):
    # Run the command to change the status of the network interface
    if action == 'up':
        subprocess.run(['nmcli', 'device', 'connect', interface_name], stdout=subprocess.PIPE)
    elif action == 'down':
        subprocess.run(['nmcli', 'device', 'disconnect', interface_name], stdout=subprocess.PIPE)

def main():
    # Get the list of interfaces and their statuses
    interfaces_status = get_interfaces_status()
    
    # Check if we found any interfaces
    if interfaces_status:
        # Print interfaces and their statuses with index numbers
        print("Interfaces and their statuses:")
        for index, (interface, state, connection, ip_address) in enumerate(interfaces_status):
            print(f"{index + 1}: {interface}: {state} (Connection: {connection}, IP: {ip_address})")
        
        # Prompt the user to enter the index of the interface
        try:
            choice = int(input("Enter the number of the interface you want to change status: ")) - 1
            if choice < 0 or choice >= len(interfaces_status):
                print("Invalid choice. Please run the script again.")
                return
        except ValueError:
            print("Invalid input. Please enter a number.")
            return

        action = input("Enter 'up' to connect or 'down' to disconnect: ")
        
        # Get the chosen interface
        interface_to_change = interfaces_status[choice][0]
        
        # Check if the action is valid
        if action in ['up', 'down']:
            # Change the status of the interface
            change_interface_status(interface_to_change, action)
            print(f"The interface {interface_to_change} has been set {action}.")
        else:
            # If the action is not valid, display a message
            print("Invalid action. Please enter 'up' or 'down'.")
    else:
        print("No interfaces were found.")

if __name__ == "__main__":
    main()