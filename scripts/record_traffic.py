import subprocess
import re
import datetime

def get_ip_address(interface_name):
    # Run the command to get the IP address of the interface
    result = subprocess.run(['ip', 'addr', 'show', interface_name], stdout=subprocess.PIPE)
    # Decode the result to text
    output = result.stdout.decode('utf-8')

    # Use a regular expression to find the IP address in the output
    ip_match = re.search(r'inet (\d+\.\d+\.\d+\.\d+)', output)
    if ip_match:
        return ip_match.group(1)
    return 'N/A'

def get_active_interfaces():
    # Get the list of active network interfaces
    result = subprocess.run(['ip', 'link', 'show', 'up'], stdout=subprocess.PIPE)
    output = result.stdout.decode('utf-8')
    interfaces = []

    # Extract the interface names
    lines = output.split('\n')
    for line in lines:
        if '<' in line and '>' in line:
            interface = line.split(': ')[1].split('@')[0]
            if interface != 'lo':  # Exclude the loopback interface
                ip_address = get_ip_address(interface)
                interfaces.append((interface, ip_address))

    return interfaces

def capture_traffic(interface, duration):
    # Construct the filename for the pcap file
    timestamp = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
    filename = f"{interface}_{timestamp}.pcap"

    # Construct the tcpdump command
    tcpdump_command = f"sudo tcpdump -i {interface} -w {filename} -G {duration} -W 1"

    print(f"Starting packet capture on interface {interface}.")
    print(f"Capturing packets for {duration} seconds.")
    subprocess.run(tcpdump_command, shell=True)
    print(f"Packet capture complete. File saved as {filename}")

def main():
    interfaces = get_active_interfaces()
    if not interfaces:
        print("No active network interfaces found.")
        return

    print("Active network interfaces:")
    for idx, (interface, ip_address) in enumerate(interfaces):
        print(f"{idx + 1}. {interface} (IP: {ip_address})")

    choice = input("Enter the number of the interface to capture traffic on: ")
    try:
        selected_interface = interfaces[int(choice) - 1][0]  # [0] to get the interface name
    except (IndexError, ValueError):
        print("Invalid selection.")
        return

    duration = input("Enter the duration of the capture in seconds: ")
    try:
        duration = int(duration)
    except ValueError:
        print("Invalid duration. Please enter a number.")
        return

    capture_traffic(selected_interface, duration)

if __name__ == "__main__":
    main()
