import subprocess

# Define the base URL for the downloads
BASE_URL = "http://ipv4.download.thinkbroadband.com/"

# Define the file size options
file_sizes = {
    "1": "1MB.zip",
    "5": "5MB.zip",
    "20": "20MB.zip",
    "50": "50MB.zip"
}

# Function to download the file
def download_file(file_size, proxy_url):
    if file_size in file_sizes:
        filename = file_sizes[file_size]
        url = BASE_URL + filename

        # Local filename to save the file
        local_filename = f"downloaded_{file_size}MB.zip"

        # Prepare curl command with write-out options for download details
        if file_size == 1:
            curl_command = f"curl -x {proxy_url} http://speedtest.ftp.otenet.gr/files/test1Mb.db -o {local_filename} --progress-bar --write-out 'Download completed.\\nTotal time: %{{time_total}}s\\nDownload speed: %{{speed_download}} bytes/sec\\n'"
        else:
            curl_command = f"curl -x {proxy_url} {url} -o {local_filename} --progress-bar --write-out 'Download completed.\\nTotal time: %{{time_total}}s\\nDownload speed: %{{speed_download}} bytes/sec\\n'"

        # Start the download
        print(f"Downloading {file_size}MB file through {proxy_url}...")
        subprocess.run(curl_command, shell=True)
    else:
        print("Invalid option. Please choose from the available options.")

def main():
    # Display the options to the user
    print("Available file sizes for download:")
    for size in file_sizes:
        print(f"{size}MB")
    
    # Get the user's choice for file size
    choice = input("Choose the file size to download (in MB): ")

    # Offer default SOCKS5 proxy or custom
    use_default_proxy = input("Do you want to use the default SOCKS5 proxy (socks5h://127.0.0.1:1080)? (y/n): ")
    if use_default_proxy.lower() == 'y':
        proxy_url = "socks5h://127.0.0.1:1080"
    else:
        proxy_url = input("Enter the SOCKS5 proxy URL (e.g., socks5h://127.0.0.1:1080): ")

    download_file(choice, proxy_url)

if __name__ == "__main__":
    main()
